import os
from contextlib import contextmanager

import sqlalchemy as sa
import sqlalchemy.ext.declarative as sa_ext_decl
from sqlalchemy import SMALLINT, VARCHAR, Column, Integer

Base = sa_ext_decl.declarative_base()


class UserTable(Base):
    __tablename__ = "USER"
    ID = Column(Integer, primary_key=True)
    NAME = Column(VARCHAR(255), primary_key=True)
    EMAIL = Column(VARCHAR(255))
    PASSWORD = Column(VARCHAR(255))
    GROUP_ID = Column(Integer)


class UserGroupTable(Base):
    __tablename__ = "USER_GROUP"
    ID = Column(Integer, primary_key=True)
    NAME = Column(VARCHAR(255), primary_key=True)


def sessionmaker():
    connection_str = "{driver}://{user}:{pwd}@{server}/{db_name}".format(
        driver="mysql+pymysql",
        user=os.getenv("SQL_USERNAME"),
        pwd=os.getenv("SQL_PASSWORD"),
        server=os.getenv("SQL_SERVER"),
        db_name=os.getenv("DB_NAME"),
    )
    debug = os.getenv("DEBUG", "false").lower() in {"true", "1"}
    engine = sa.create_engine(connection_str, echo=debug)

    @sa.event.listens_for(engine, "before_cursor_execute")
    def dummy_receive_before_cursor_execute(
        conn, cursor, statement, params, context, executemany
    ):
        if executemany:
            cursor.fast_executemany = True

    return sa.orm.sessionmaker(bind=engine)


# exposed for other modules:
Session = sessionmaker()


@contextmanager
def session_scope():
    """access sessions with with statement"""
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
