from pydantic import BaseModel


class Group(BaseModel):
    uid: str
    name: str


class User(BaseModel):
    uid: str
    name: str
    email: str
    group: str


class UserPassword(User):
    password: str
