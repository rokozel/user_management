from typing import List, Optional

from fastapi import Depends, FastAPI, status
from fastapi.responses import Response

from user_management.access_group import GroupAccess
from user_management.access_user import UserAccess
from user_management.schemas import Group, User


def get_user_access() -> UserAccess:
    return UserAccess()


def get_group_access() -> GroupAccess:
    return GroupAccess()


def fake_hash_password(password: str):
    return "fakehashed" + password


def app_factory():
    """Define and return the main FastAPI app"""
    app = FastAPI(
        title="Backend Server",
        description="""User management api and database access""",
        version="0.1.0",
    )

    @app.post(
        "/user",
        status_code=status.HTTP_201_CREATED,
    )
    async def create_user(
        name: str,
        email: str,
        password: str,
        group: str,
        group_access: UserAccess = Depends(get_group_access),
        user_access: UserAccess = Depends(get_user_access),
    ):
        db_group = group_access.get_group(name=group)[0]
        hashed_password = fake_hash_password(password)
        user_access.create_user(
            name=name,
            email=email,
            password=hashed_password,
            group_id=db_group.uid,
        )

    @app.get(
        "/user",
        responses={
            "200": {"model": List[User]},
        },
    )
    async def get_user(
        name: Optional[str] = None,
        email: Optional[str] = None,
        group: Optional[str] = None,
        user_access: UserAccess = Depends(get_user_access),
    ):
        return user_access.get_user(name=name, email=email, group=group)

    @app.put(
        "/user",
        status_code=status.HTTP_204_NO_CONTENT,
    )
    async def update_user(
        uid: str,
        name: Optional[str] = None,
        email: Optional[str] = None,
        password: Optional[str] = None,
        group: Optional[str] = None,
        group_access: UserAccess = Depends(get_group_access),
        user_access: UserAccess = Depends(get_user_access),
    ):
        db_group = group_access.get_group(name=group)[0]
        hashed_password = None
        if password is not None:
            hashed_password = fake_hash_password(password)

        user_access.update_user(
            uid=uid,
            name=name,
            email=email,
            password=hashed_password,
            group_id=db_group.uid,
        )
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    @app.delete(
        "/user",
        status_code=status.HTTP_204_NO_CONTENT,
    )
    async def remove_user(
        uid: str,
        user_access: UserAccess = Depends(get_user_access),
    ):
        user_access.delete_user(uid=uid)
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    @app.post(
        "/group",
        status_code=status.HTTP_201_CREATED,
    )
    async def create_group(
        name: str,
        group_access: GroupAccess = Depends(get_group_access),
    ):
        group_access.create_group(name=name)

    @app.get(
        "/group",
        responses={
            "200": {"model": List[Group]},
        },
    )
    async def get_group(
        name: Optional[str] = None,
        group_access: GroupAccess = Depends(get_group_access),
    ):
        return group_access.get_group(name=name)

    @app.put(
        "/group",
        status_code=status.HTTP_204_NO_CONTENT,
    )
    async def update_group(
        uid: str,
        name: str,
        group_access: GroupAccess = Depends(get_group_access),
    ):
        group_access.update_group(uid=uid, name=name)
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    @app.delete(
        "/group",
        status_code=status.HTTP_204_NO_CONTENT,
    )
    async def delete_group(
        uid: str,
        group_access: GroupAccess = Depends(get_group_access),
    ):
        group_access.delete_group(uid=uid)
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    return app


def main():
    app = app_factory()

    return app


if __name__ == "__main__":

    import uvicorn

    uvicorn.run(main, host="0.0.0.0", port=8000, factory=True)
