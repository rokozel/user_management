from typing import List, Optional

from user_management.connector_db import (
    UserGroupTable,
    UserTable,
    session_scope,
)
from user_management.schemas import User


class UserAccess:
    @staticmethod
    def create_user(name: str, email: str, password: str, group_id: int):
        with session_scope() as session:
            user = UserTable(
                NAME=name,
                EMAIL=email,
                PASSWORD=password,
                GROUP_ID=group_id,
            )
            session.add(user)

    def get_user(
        self,
        name: Optional[str] = None,
        email: Optional[str] = None,
        group: Optional[str] = None,
    ) -> List[User]:
        with session_scope() as session:
            q = session.query(UserTable, UserGroupTable).filter(
                UserTable.GROUP_ID == UserGroupTable.ID
            )

            if name is not None:
                q = q.filter(UserTable.NAME == name)

            if email is not None:
                q = q.filter(UserTable.EMAIL == email)

            if group is not None:
                q = q.filter(UserGroupTable.NAME == group)

        return [
            User(
                uid=user[UserTable].ID,
                name=user[UserTable].NAME,
                email=user[UserTable].EMAIL,
                group=user[UserGroupTable].NAME,
            )
            for user in q.all()
        ]

    @staticmethod
    def update_user(
        uid: str,
        name: Optional[str] = None,
        email: Optional[str] = None,
        password: Optional[str] = None,
        group_id: Optional[int] = None,
    ):
        new_user = {
            "NAME": name,
            "EMAIL": email,
            "PASSWORD": password,
            "GROUP_ID": group_id,
        }
        with session_scope() as session:
            q = (
                session.query(UserTable)
                .filter_by(ID=uid)
                .update(
                    {k: v for k, v in new_user.items() if v is not None},
                    synchronize_session="fetch",
                )
            )

    @staticmethod
    def delete_user(uid: str):
        with session_scope() as session:
            q = (
                session.query(UserTable)
                .filter_by(ID=uid)
                .delete(synchronize_session="fetch")
            )


class MockUserAccess:
    def __init__(self, users: List[User]):
        self.users = users

    @staticmethod
    def create_user(name: str, email: str, password: str, group_id: int):
        return None

    def get_user(
        self,
        name: Optional[str] = None,
        email: Optional[str] = None,
        group: Optional[str] = None,
    ) -> List[User]:
        result = [
            user
            for user in self.users
            if (
                name in [user.name, None]
                and email in [user.email, None]
                and group in [user.group, None]
            )
        ]
        return result

    @staticmethod
    def update_user(
        uid: str,
        name: Optional[str] = None,
        email: Optional[str] = None,
        password: Optional[str] = None,
        group_id: Optional[int] = None,
    ):
        return None

    @staticmethod
    def delete_user(uid: str):
        return None
