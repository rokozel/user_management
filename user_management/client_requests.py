import os
from typing import List, Optional
from urllib.parse import urljoin

import requests

from user_management.schemas import Group, User

BASE_URL = os.environ.get("BACKEND_URL", "http://0.0.0.0:8000")


def create_user(name: str, password: str, email: str, group: str):

    response = requests.post(
        url=urljoin(BASE_URL, "user"),
        params={
            "name": name,
            "password": password,
            "email": email,
            "group": group,
        },
    )
    response.raise_for_status()


def get_user(
    name: Optional[str] = None,
    email: Optional[str] = None,
    group: Optional[str] = None,
) -> List[User]:

    response = requests.get(
        url=urljoin(BASE_URL, "user"),
        params={
            "name": name,
            "email": email,
            "group": group,
        },
    )
    response.raise_for_status()
    return [User(**user) for user in response.json()]


def update_user(
    uid: str,
    name: Optional[str] = None,
    email: Optional[str] = None,
    password: Optional[str] = None,
    group: Optional[str] = None,
):

    response = requests.put(
        url=urljoin(BASE_URL, "user"),
        params={
            "uid": uid,
            "name": name,
            "email": email,
            "password": password,
            "group": group,
        },
    )
    response.raise_for_status()


def delete_user(
    uid: str,
):

    response = requests.delete(
        url=urljoin(BASE_URL, "user"),
        params={
            "uid": uid,
        },
    )
    response.raise_for_status()


def create_group(name: str):

    response = requests.post(
        url=urljoin(BASE_URL, "group"),
        params={
            "name": name,
        },
    )
    response.raise_for_status()


def get_group(name: Optional[str] = None) -> List[Group]:

    response = requests.get(
        url=urljoin(BASE_URL, "group"),
        params={
            "name": name,
        },
    )
    response.raise_for_status()
    return [Group(**group) for group in response.json()]


def update_group(uid: str, name: Optional[str] = None):

    response = requests.put(
        url=urljoin(BASE_URL, "group"),
        params={
            "uid": uid,
            "name": name,
        },
    )
    response.raise_for_status()


def delete_group(
    uid: str,
):

    response = requests.delete(
        url=urljoin(BASE_URL, "group"),
        params={
            "uid": uid,
        },
    )
    response.raise_for_status()
