from typing import List, Optional

from user_management.connector_db import UserGroupTable, session_scope
from user_management.schemas import Group


class GroupAccess:
    @staticmethod
    def create_group(name: str):
        with session_scope() as session:
            session.add(UserGroupTable(NAME=name))

    @staticmethod
    def get_group(name: Optional[str] = None) -> List[Group]:
        with session_scope() as session:
            q = session.query(UserGroupTable)

            if name is not None:
                q = q.filter_by(NAME=name)

        return [Group(uid=group.ID, name=group.NAME) for group in q.all()]

    @staticmethod
    def update_group(uid: str, name: str):
        with session_scope() as session:
            q = (
                session.query(UserGroupTable)
                .filter_by(ID=uid)
                .update({"NAME": name}, synchronize_session="fetch")
            )

    @staticmethod
    def delete_group(uid: str):
        with session_scope() as session:
            q = (
                session.query(UserGroupTable)
                .filter_by(ID=uid)
                .delete(synchronize_session="fetch")
            )


class MockGroupAccess:
    def __init__(self, groups: List[Group]):
        self.groups = groups

    @staticmethod
    def create_group(name: str):
        return None

    def get_group(self, name: Optional[str] = None) -> List[Group]:
        result = [group for group in self.groups if name in [group.name, None]]
        return result

    @staticmethod
    def update_group(uid: str, name: str):
        return None

    @staticmethod
    def delete_group(uid: str):
        return None
