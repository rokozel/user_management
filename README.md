# User Management

## Project Documentation
[Project Specifications](https://docs.google.com/document/d/1wrYOAhD5zTGajshkJMOCQGC43y1f0f5mjIG8riblpTY/edit?usp=sharing)

## To start service

### Create common network for dockers
docker network create user_management_network


### Start database
cd db
docker compose -f docker-compose.yml up


### Start service
cd to project root
docker compose -f docker-compose.yml up


### Run (integration) tests
cd to project root
docker compose -f docker-compose-tests.yml up


### Access openapi specifications
[OpenAPI Specification 3](http://localhost:8000/openapi.json)  
[OpenAPI Docs ](http://localhost:8000/docs)