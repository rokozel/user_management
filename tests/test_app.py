import pytest
from fastapi.testclient import TestClient

from user_management.access_group import MockGroupAccess
from user_management.access_user import MockUserAccess
from user_management.app import app_factory, get_group_access, get_user_access
from user_management.schemas import Group, User

users = [
    User(uid="1923", name="JanezNovak", email="janez@gmail.com", group="3fs"),
    User(uid="1924", name="MarijaHorvat", email="marija@t.com", group="meta"),
]

groups = [Group(uid="1925", name="3fs"), Group(uid="1926", name="meta")]


@pytest.fixture
def mock_app():
    app = app_factory()
    app.dependency_overrides[get_user_access] = lambda: MockUserAccess(users)
    app.dependency_overrides[get_group_access] = lambda: MockGroupAccess(groups)
    return app


def test_create_user(mock_app):
    client = TestClient(mock_app)

    response = client.post(
        "/user",
        params={
            "name": "JanezNovak",
            "password": "N4ky?abrUB2Z7",
            "email": "janez@gmail.com",
            "group": "3fs",
        },
    )

    assert response.status_code == 201


def test_get_user(mock_app):
    client = TestClient(mock_app)

    response = client.get("/user")

    assert response.status_code == 200
    assert isinstance(response.json(), list)
    assert all(isinstance(User(**r), User) for r in response.json())


def test_update_user(mock_app):
    client = TestClient(mock_app)

    response = client.put(
        "/user",
        params={
            "uid": "1923",
            "name": "NewName",
        },
    )

    assert response.status_code == 204


def test_delete_user(mock_app):
    client = TestClient(mock_app)

    response = client.delete(
        "/user",
        params={"uid": "1923"},
    )

    assert response.status_code == 204


def test_create_group(mock_app):
    client = TestClient(mock_app)

    response = client.post(
        "/group",
        params={"name": "3fs"},
    )

    assert response.status_code == 201


def test_get_group(mock_app):
    client = TestClient(mock_app)

    response = client.get("/group")

    assert response.status_code == 200
    assert isinstance(response.json(), list)
    assert all(isinstance(Group(**r), Group) for r in response.json())


def test_update_group(mock_app):
    client = TestClient(mock_app)

    response = client.put(
        "/group",
        params={
            "uid": "1923",
            "name": "NewGroupName",
        },
    )

    assert response.status_code == 204


def test_delete_group(mock_app):
    client = TestClient(mock_app)

    response = client.delete(
        "/group",
        params={"uid": "1923"},
    )

    assert response.status_code == 204


if __name__ == "__main__":
    import sys

    pytest.main(sys.argv + ["-v"])
