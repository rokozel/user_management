import pytest

from user_management import client_requests
from user_management.schemas import User
from user_management.settings import logger

users = [
    {
        "name": "JanezNovak",
        "email": "janez@gmail.com",
        "password": "superSecurePsw",
        "group": "3fs",
    },
    {
        "name": "MarijaHorvat",
        "email": "marija@t.com",
        "password": "superSecurePsw2",
        "group": "meta",
    },
    {
        "name": "MarkoKovac",
        "email": "marko.kovac@t.com",
        "password": "superSecurePsw3",
        "group": "nokia",
    },
]

groups = [{"name": "3fs"}, {"name": "meta"}, {"name": "nokia"}]


def delete_groups():
    logger.info("Delete all groups and users in db")
    db_users = client_requests.get_user()

    for user in db_users:
        client_requests.delete_user(uid=user.uid)

    db_groups = client_requests.get_group()
    for group in db_groups:
        client_requests.delete_group(uid=group.uid)


@pytest.fixture(autouse=True, scope="session")
def prepare_database():
    delete_groups()

    logger.info(f"Insert groups in db: {groups}")
    for group in groups:
        client_requests.create_group(**group)

    logger.info(f"Insert users in db: {users}")
    for user in users:
        client_requests.create_user(**user)

    yield

    delete_groups()


def test_get_group_from_db():
    group_name = groups[0]["name"]
    group = client_requests.get_group(name=groups[0]["name"])

    assert len(group) == 1
    assert group[0].name == group_name


def test_get_all_groups_from_db():
    db_groups = client_requests.get_group()

    assert len(db_groups) == len(groups)


def test_get_all_groups_from_db():
    db_groups = client_requests.get_group()

    assert len(db_groups) == len(groups)


def test_get_user_from_db():
    db_user_by_name = client_requests.get_user(name=users[0]["name"])
    db_user_by_email = client_requests.get_user(email=users[0]["email"])
    db_user_by_group = client_requests.get_user(group=users[0]["group"])

    assert db_user_by_name == db_user_by_email == db_user_by_group


def test_get_all_users_from_db():
    db_users = client_requests.get_user()

    assert len(db_users) == len(users)


def test_update_group_in_db():
    new_name = "3fs_new_group"
    db_group = client_requests.get_group(name=groups[1]["name"])
    client_requests.update_group(uid=db_group[0].uid, name=new_name)

    new_db_group = client_requests.get_group(name=new_name)

    assert len(new_db_group) == 1
    assert new_name == new_db_group[0].name


def test_update_user_in_db():
    new_user = {
        "name": "NEWMarijaHorvat",
        "email": "NEWmarija@t.com",
        "password": "NEWsuperSecurePsw2",
        "group": groups[2]["name"],
    }

    db_user = client_requests.get_user(
        name=users[2]["name"],
        email=users[2]["email"],
        group=users[2]["group"],
    )
    client_requests.update_user(
        **{
            "uid": db_user[0].uid,
            "name": new_user["name"],
            "email": new_user["email"],
            "password": new_user["password"],
            "group": new_user["group"],
        }
    )
    new_db_user = client_requests.get_user(name=new_user["name"])

    assert len(new_db_user) == 1
    assert User(**{**new_user, **{"uid": new_db_user[0].uid}}) == new_db_user[0]
