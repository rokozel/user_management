from user_management import schemas


def test_user():
    result = schemas.User(
        uid="243681",
        name="JanezNovak",
        email="janez.novak@3fs.com",
        group="3fs",
    )
    assert all(
        hasattr(result, attr) for attr in ["uid", "name", "email", "group"]
    )


def test_userpassword():
    result = schemas.UserPassword(
        uid="243681",
        name="JanezNovak",
        email="janez.novak@3fs.com",
        group="3fs",
        password="93ddba89976c5c1a3642a0204b7d094ff3fd79c60e600e9e8fd1d7061a9098ea",
    )
    assert all(
        hasattr(result, attr)
        for attr in ["uid", "name", "email", "group", "password"]
    )


def test_group():
    result = schemas.Group(
        uid="243681",
        name="3fs",
    )
    assert all(hasattr(result, attr) for attr in ["uid", "name"])
