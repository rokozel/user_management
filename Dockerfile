FROM --platform=linux/amd64 python:3.10-bullseye AS user_management

ENV POETRY_VERSION=1.1.13
ENV PYTHONPATH=/user_management:$PYTHONPATH

# System deps:
RUN pip install "poetry==${POETRY_VERSION}" virtualenv

# Copy only requirements to cache them in docker layer
WORKDIR /user_management
COPY pyproject.toml poetry.lock ./

# Project initialization:
RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-root
COPY user_management ./user_management
COPY tests ./tests

EXPOSE 8000